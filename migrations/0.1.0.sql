BEGIN;

CREATE TABLE IF NOT EXISTS passwords (
       id            uuid PRIMARY KEY,
       login         character varying(64) NOT NULL,
       password      character varying(64),
       site          character varying(128) NOT NULL,
       UNIQUE        (login, site),
       q             tsvector
);

CREATE INDEX IF NOT EXISTS passwords_login_site
       ON passwords
       USING btree(login, site);

CREATE INDEX IF NOT EXISTS passwords_site_login
       ON passwords
       USING btree(site, login);

CREATE INDEX IF NOT EXISTS q_idx
       ON passwords USING gin(q);

DROP TRIGGER IF EXISTS passords_q_update ON passwords;

CREATE OR REPLACE FUNCTION passwords_search_trigger() RETURNS trigger AS $$
begin
  new.q :=
    setweight(to_tsvector(coalesce(new.login,'')), 'D') ||
    setweight(to_tsvector(coalesce(new.site,'')), 'A');
  return new;
end
$$ LANGUAGE plpgsql;

CREATE TRIGGER passords_q_update BEFORE INSERT OR UPDATE
       ON passwords FOR EACH ROW EXECUTE PROCEDURE passwords_search_trigger();

COMMIT;
