Mypasswords API
===============

.. image:: https://travis-ci.org/toopy/mypasswords-api.svg?branch=master
    :target: https://travis-ci.org/toopy/mypasswords-api

API for your passwords.


Install
-------

This python project requires `python 3.6`, otherwise the `pip install` should
work fine:

.. code:: bash

    $ pip install mypasswords-api


Usage
-----

By default we can run the server as `python` module as follow:

.. code:: bash

   $ mypasswords_api 127.0.0.1:8000
   ======== Running on http://127.0.0.1:8000 ========
   (Press CTRL+C to quit)


It will start an `aiohttp` instance with the `asyncio`, or `uvloop` (if
installed), event loop.


Settings
--------

To handle settings we use `simple-settings` composition behavior. By default
the base settings module will try to obtain values from the current environment:

.. code:: bash

   $ MYPASSWORDS_PER_PAGE=100 PG_HOST=localhost mypasswords_api 127.0.0.1:8000


API
---

Create
^^^^^^

.. code:: bash

   $ http POST http://127.0.0.1:8000/passwords login=toopy password=abcd1234 site=github.com
   HTTP/1.1 201 Created
   Content-Length: 110
   Content-Type: application/json; charset=utf-8
   Date: Sat, 09 Sep 2017 08:59:37 GMT
   Server: Python/3.6 aiohttp/2.2.0
   X-API-Version: 0.1.0.dev0

   {
       "id": "0753c165-0400-4e1f-b79c-8998d4987daf",
       "login": "toopy",
       "password": "abcd1234",
       "site": "github.com"
   }


Update
^^^^^^

.. code:: bash

   $ http PUT http://127.0.0.1:8000/passwords/0753c165-0400-4e1f-b79c-8998d4987daf login=toopy password=1234abcd site=github.com
   HTTP/1.1 200 OK
   Content-Length: 76
   Content-Type: application/json; charset=utf-8
   Date: Sat, 09 Sep 2017 09:05:47 GMT
   Server: Python/3.6 aiohttp/2.2.0
   X-API-Version: 0.1.0.dev0

   {
       "id": null,
       "login": "toopy",
       "password": "1234abcd",
       "site": "github.com"
   }


List
^^^^

.. code:: bash

   $ http GET "http://127.0.0.1:8000/passwords?direction=desc&order_by=site&page1&per_page=2"
   HTTP/1.1 200 OK
   Content-Length: 212
   Content-Type: application/json; charset=utf-8
   Date: Sat, 09 Sep 2017 09:23:43 GMT
   Server: Python/3.6 aiohttp/2.2.0
   X-API-Version: 0.1.0.dev0
   X-Next-Link: /passwords?direction=desc&order_by=site&page=1&per_page=2

   [
       {
           "id": "f4a59e0f-f1ff-4bc7-8757-203fc4b0aff7",
           "login": "toopy",
           "password": null,
           "site": "twitter.com"
       },
       {
           "id": "7ef3130f-7844-49c6-a46c-2f6960cae697",
           "login": "toopy",
           "password": null,
           "site": "slack.com"
       }
   ]


Search
^^^^^^

.. code:: bash

   $ http GET "http://127.0.0.1:8000/passwords?q=git"
   HTTP/1.1 200 OK
   Content-Length: 106
   Content-Type: application/json; charset=utf-8
   Date: Sat, 09 Sep 2017 09:46:26 GMT
   Server: Python/3.6 aiohttp/2.2.0
   X-API-Version: 0.1.0.dev0

   [
       {
           "id": "0753c165-0400-4e1f-b79c-8998d4987daf",
           "login": "toopy",
           "password": null,
           "site": "github.com"
       }
   ]



Get
^^^

.. code:: bash

   $ http GET http://127.0.0.1:8000/passwords/7ef3130f-7844-49c6-a46c-2f6960cae697
   HTTP/1.1 200 OK
   Content-Length: 109
   Content-Type: application/json; charset=utf-8
   Date: Sat, 09 Sep 2017 09:24:33 GMT
   Server: Python/3.6 aiohttp/2.2.0
   X-API-Version: 0.1.0.dev0

   {
       "id": "7ef3130f-7844-49c6-a46c-2f6960cae697",
       "login": "toopy",
       "password": "1234abcd",
       "site": "slack.com"
   }


Head
^^^^

.. code:: bash

   $ http HEAD http://127.0.0.1:8000/passwords/7ef3130f-7844-49c6-a46c-2f6960cae697
   HTTP/1.1 200 OK
   Content-Length: 0
   Content-Type: application/json
   Date: Sat, 09 Sep 2017 09:24:46 GMT
   Server: Python/3.6 aiohttp/2.2.0
   X-API-Version: 0.1.0.dev0


Delete
^^^^^^

.. code:: bash

   $ http DELETE http://127.0.0.1:8000/passwords/7ef3130f-7844-49c6-a46c-2f6960cae697
   HTTP/1.1 204 No Content
   Content-Length: 2
   Content-Type: application/json; charset=utf-8
   Date: Sat, 09 Sep 2017 09:46:55 GMT
   Server: Python/3.6 aiohttp/2.2.0
   X-API-Version: 0.1.0.dev0


License
-------

MIT License
