import argparse
import asyncio
import logging.config
import os

import aiolog
from aiohttp import web
from asyncpg import connect as connect_pg
from simple_settings import settings

from .healthchecks.routes import register_routes as register_heathchecks_routes
from .middlewares.cors import cors_middleware
from .middlewares.pagination import pagination_middleware
from .middlewares.version import version_middleware
from .resources.passwords.routes import register_routes as register_passwords_routes

try:
    import uvloop

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    pass

os.environ.setdefault("SIMPLE_SETTINGS", "mypasswords_api.settings.base")


async def cleanup_plugins(app):
    await app.conn.close()


def get_middlewares():
    return [
        cors_middleware,
        pagination_middleware,
        version_middleware,
    ]


async def load_plugins(app):
    app.conn = await connect_pg(
        database=settings.PGNAME,
        host=settings.PGHOST,
        port=settings.PGPORT,
        user=settings.PGUSER,
        password=settings.PGPASS,
    )


def register_routes(app):
    register_heathchecks_routes(app)
    register_passwords_routes(app)


async def setup_logging(app):
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {"default": {"format": "%(message)s",},},
            "handlers": {
                "console": {"class": "logging.StreamHandler", "formatter": "default",},
            },
            "root": {"handlers": ["console"], "level": "INFO",},
        }
    )
    aiolog.setup_aiohttp(app)


def build_app():
    app = web.Application(middlewares=get_middlewares())
    app.on_startup.append(setup_logging)
    app.on_startup.append(load_plugins)
    app.on_cleanup.append(cleanup_plugins)
    register_routes(app)
    return app


def serve(host="127.0.0.1", port=8000):
    web.run_app(build_app(), host=host, port=int(port))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("host", nargs="?")

    args = parser.parse_args()
    host = (args.host or "127.0.0.1:8000").split(":")

    serve(*host)
