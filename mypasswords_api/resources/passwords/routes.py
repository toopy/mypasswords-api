from .views import PasswordsView


def register_routes(app):
    app.router.add_route("*", "/passwords", PasswordsView)
    app.router.add_route("*", "/passwords/{id}", PasswordsView)
