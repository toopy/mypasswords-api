from json.decoder import JSONDecodeError

import jwt
from aiohttp import web
from mypasswords_api.core.exceptions import (
    NotFound,
    UniqueViolationError,
    ValidationError,
)
from mypasswords_api.core.jwt import decode_jwt, encode_jwt
from mypasswords_api.core.password import Password


class PasswordsView(web.View):
    @property
    def conn(self):
        return self.request.app.conn

    @property
    def not_found(self):
        return web.json_response({"error": {"message": "not-found"}}, status=404,)

    async def delete(self):
        password_id = self.request.match_info.get("id")

        if not password_id:
            return self.not_found

        try:
            await Password.delete(self.conn, password_id)
            return web.json_response({}, status=204)

        except NotFound:
            return self.not_found

    async def get(self):
        password_id = self.request.match_info.get("id")
        q = self.request.query.get("q")

        if not password_id:
            passwords = await Password.list(
                self.conn,
                direction=self.request.direction,
                order_by=self.request.order_by,
                page=self.request.page,
                per_page=self.request.per_page,
                q=q,
            )
            return web.json_response([p.to_dict() for p in passwords])

        try:
            password = await Password.get(self.conn, password_id)
            encoded = await encode_jwt(password.to_dict())
            return web.json_response({"jwt": encoded})
        except NotFound:
            return self.not_found

    async def head(self):
        password_id = self.request.match_info.get("id")
        q = self.request.query.get("q")

        if not password_id:
            count = await Password.count(self.conn, q=q)
            return web.json_response(headers={"X-Count": str(count)}, status=200)

        try:
            await Password.exist(self.conn, password_id)
            return web.json_response(status=200)

        except NotFound:
            return web.json_response(status=404)

    async def _create_or_update(self):
        try:
            encoded = (await self.request.json())["jwt"]
            data = await decode_jwt(encoded)

            password_id = self.request.match_info.get("id")
            password = Password(**{"id": password_id, **data})

            # check exists ?
            if password_id:
                try:
                    await Password.exist(self.conn, password_id)
                except NotFound:
                    password_id = None

            if password_id:
                await password.update(self.conn, password_id)
                status = 200
            else:
                await password.create(self.conn)
                status = 201

            return web.json_response(password.to_dict(), status=status,)

        except jwt.exceptions.DecodeError:
            return web.json_response(
                {"error": {"message": "jwt-decode-error",}}, status=400
            )

        except JSONDecodeError:
            return web.json_response(
                {"error": {"message": "validation-error",}}, status=400
            )

        except UniqueViolationError:
            return web.json_response(
                {"error": {"message": "unique-constraint-error",}}, status=400
            )

        except ValidationError as e:
            return web.json_response(
                {
                    "error": {
                        "message": "validation-error",
                        "fields": {
                            ",".join(err["loc"]): err["msg"] for err in e.errors()
                        },
                    }
                },
                status=400,
            )

        except Exception:
            return web.json_response(
                {"error": {"message": "server-error",}}, status=500
            )

    async def options(self):
        return web.json_response(status=200)

    async def post(self):
        return await self._create_or_update()

    async def put(self):
        return await self._create_or_update()
