import os

from .base import *  # noqa

CLIENT_CRT_LIST = os.getenv("MYPASSWORDS_CLIENT_CRT_LIST", "data/client.crt")
SERVER_KEY = os.getenv("MYPASSWORDS_SERVER_KEY", "data/server.key")
