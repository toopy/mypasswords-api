import os

# debug

MYPASSWORDS_DEBUG = os.getenv("MYPASSWORDS_DEBUG", "false") == "true"

# api

MYPASSWORDS_PER_PAGE = os.getenv("MYPASSWORDS_PER_PAGE", 25)
MYPASSWORDS_PG_TABLE = os.getenv("MYPASSWORDS_PG_TABLE", "passwords")

# database

PGHOST = os.getenv("PGHOST", "127.0.0.1")
PGNAME = os.getenv("PGNAME", "mypasswords")
PGPORT = os.getenv("PGPORT", 5432)
PGUSER = os.getenv("PGUSER", "mypasswords")
PGPASS = os.getenv("PGPASSWORD", "mypasswords")

# encryption

CLIENT_CRT_LIST = os.getenv("MYPASSWORDS_CLIENT_CRT_LIST")
SERVER_KEY = os.getenv("MYPASSWORDS_SERVER_KEY")
