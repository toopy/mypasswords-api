import os

import aiofiles
import jwt
from simple_settings import settings

from .exceptions import ServerError


async def get_client_crt_list():
    client_crt_list = []

    for path in filter(None, settings.CLIENT_CRT_LIST.split(",")):
        if not path or not os.path.exists(path):
            continue
        async with aiofiles.open(path) as f:
            client_crt_list.append(await f.read())

    if not client_crt_list:
        raise ServerError

    return client_crt_list


async def get_server_key():
    if not settings.SERVER_KEY or not os.path.exists(settings.SERVER_KEY):
        raise ServerError

    async with aiofiles.open(settings.SERVER_KEY) as f:
        return await f.read()


async def decode_jwt(encoded):
    client_crt_list = await get_client_crt_list()
    for client_crt in client_crt_list:
        try:
            return jwt.decode(encoded, client_crt, algorithms="RS256")
        except Exception as e:
            failed = e
    raise failed or ServerError


async def encode_jwt(data):
    server_key = await get_server_key()
    return jwt.encode(data, server_key, algorithm="RS256").decode()
