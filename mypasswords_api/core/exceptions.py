from asyncpg.exceptions import UniqueViolationError
from pydantic import ValidationError


class NotFound(Exception):
    pass


class ServerError(Exception):
    pass


__all__ = [
    "NotFound",
    "ServerError",
    "UniqueViolationError",
    "ValidationError",
]
