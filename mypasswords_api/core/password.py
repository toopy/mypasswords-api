import logging
from uuid import UUID, uuid4

from pydantic import BaseModel, Required
from simple_settings import settings

from .exceptions import NotFound

logger = logging.getLogger(__name__)


class Uuid:
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        if isinstance(value, str):
            try:
                UUID(value)
            except (AttributeError, TypeError, ValueError):
                raise ValueError(f"uuid: {value} is not a valid uuid")
        elif not isinstance(value, UUID):
            raise ValueError(f"uuid: uuid expected not {type(value)}")
        return value


class Password(BaseModel):
    id: Uuid = None
    login: str = Required
    password: str = None
    site: str = Required

    class Config:
        arbitrary_types_allowed = True

    @classmethod
    async def count(cls, conn, q=None):
        query = "SELECT count(*) FROM {}".format(settings.MYPASSWORDS_PG_TABLE)
        if q:
            query += " WHERE q @@ '{}:*'::tsquery".format(q.lower())
        query += ";"

        result = await conn.fetchrow(query)
        return dict(result)["count"]

    async def create(self, conn):
        self.id = self.id or uuid4()
        await conn.execute(
            "INSERT INTO {} (id, login, password, site) "
            "VALUES ($1, $2, $3, $4);".format(settings.MYPASSWORDS_PG_TABLE),
            str(self.id),
            self.login,
            self.password,
            self.site,
        )

    @classmethod
    async def delete(cls, conn, password_id):
        try:
            result = await conn.execute(
                "DELETE FROM {} WHERE id = $1;".format(settings.MYPASSWORDS_PG_TABLE),
                password_id,
            )

        except ValueError as e:
            logger.error("Invalid password id: %s", e)
            raise NotFound

        if result != "DELETE 1":
            logger.error("Password %s not found: %s", password_id, result)
            raise NotFound

    @classmethod
    async def exist(cls, conn, password_id):
        try:
            row = await conn.fetchrow(
                "SELECT 1 FROM {} WHERE id = $1 LIMIT 1;".format(
                    settings.MYPASSWORDS_PG_TABLE
                ),
                password_id,
            )

        except ValueError as e:
            logger.error("Invalid password id: %s", e)
            raise NotFound

        if not row:
            logger.error("Password %s not found", password_id)
            raise NotFound

        return True

    @classmethod
    async def get(cls, conn, password_id):
        try:
            row = await conn.fetchrow(
                "SELECT * FROM {} WHERE id = $1;".format(settings.MYPASSWORDS_PG_TABLE),
                password_id,
            )

        except ValueError as e:
            logger.error("Invalid password id: %s", e)
            raise NotFound

        if not row:
            raise NotFound

        return cls(**dict(row.items()))

    @classmethod
    async def list(
        cls, conn, direction="ASC", page=0, order_by="site", per_page=None, q=None
    ):
        query = "SELECT id, login, site FROM {}".format(settings.MYPASSWORDS_PG_TABLE)

        if q:
            query += " WHERE q @@ $1::tsquery"

        if order_by == "login":
            query += " ORDER BY (login, site) {}".format(direction)
        else:
            query += " ORDER BY (site, login) {}".format(direction)

        per_page = int(per_page or settings.MYPASSWORDS_PER_PAGE)
        query += " OFFSET {}".format(page * per_page)
        query += " LIMIT {}".format(per_page)
        query += ";"

        fetch_args = [query]
        if q:
            fetch_args.append("{}:*".format(q.lower()))

        rows = await conn.fetch(*fetch_args)
        return [cls(**dict(r.items())) for r in rows]

    def to_dict(self):
        return {k: v if not v else str(v) for k, v in self.dict().items()}

    async def update(self, conn, password_id):
        try:
            result = await conn.execute(
                "UPDATE {} "
                "SET login = $1, password = $2, site = $3 "
                "WHERE id = $4;".format(settings.MYPASSWORDS_PG_TABLE),
                self.login,
                self.password,
                self.site,
                password_id,
            )

        except ValueError as e:
            logger.error("Invalid password id: %s", e)
            raise NotFound

        if result != "UPDATE 1":
            logger.error("Password %s update failed: %s", self.id, result)
            raise NotFound
