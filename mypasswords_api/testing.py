from collections import namedtuple
from unittest.mock import MagicMock

FakeApp = namedtuple("FakeApp", "conn")
FakeRelUrl = namedtuple("FakeRelUrl", "name")


class AsyncMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)


class FakeRequest:
    def __init__(self, **kwargs):

        # core part
        self.app = FakeApp("dummy_conn")
        self.query = {
            "direction": "ASC",
            "order_by": "site",
            "page": 0,
            "per_page": 25,
        }
        self.match_info = {}
        self.method = "GET"
        self.rel_url = FakeRelUrl("passwords")

        # custome part
        self.direction = "ASC"
        self.order_by = "site"
        self.page = 0
        self.per_page = 25

        self.__dict__.update(**kwargs)


class FakeResponse:
    def __init__(self):
        self.headers = {}
