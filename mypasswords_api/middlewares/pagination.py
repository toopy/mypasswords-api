from functools import partial

from aiohttp.web import middleware
from mypasswords_api.core.constants import ORDER_DIRECTION
from mypasswords_api.core.password import Password
from simple_settings import settings


async def get_link(request, kind="next"):
    count = None
    new_page = request.page

    if kind == "next":
        new_page += 1
        count = await Password.count(request.app.conn, q=request.query.get("q"))
    else:
        new_page -= 1

    if (kind == "previous" and not request.page) or (
        kind == "next" and count <= new_page * request.per_page
    ):
        return

    return "/passwords?direction={}&order_by={}&page={}&per_page={}".format(
        request.direction.lower(), request.order_by, new_page, request.per_page
    )


@middleware
async def pagination_middleware(request, handler):
    # nothing to paginate
    if (
        request.method != "GET"
        or request.rel_url.name != "passwords"
        or "id" in request.match_info
    ):
        return await handler(request)

    request.direction = ORDER_DIRECTION.get(request.query.get("direction", "")) or "ASC"
    request.order_by = request.query.get("order_by", "site")
    request.page = int(request.query.get("page", 0))
    request.per_page = int(
        request.query.get("per_page", settings.MYPASSWORDS_PER_PAGE,)
    )

    response = await handler(request)

    get_link_func = partial(get_link, request)
    next_link = await get_link_func(kind="next")
    if next_link:
        response.headers["X-Next-Link"] = next_link

    previous_link = await get_link_func(kind="previous")
    if previous_link:
        response.headers["X-Previous-Link"] = previous_link

    return response
