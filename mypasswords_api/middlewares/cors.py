from aiohttp.web import middleware
from simple_settings import settings


@middleware
async def cors_middleware(request, handler):
    response = await handler(request)
    if settings.MYPASSWORDS_DEBUG:
        response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Headers"] = "Content-Type"
    response.headers[
        "Access-Control-Expose-Headers"
    ] = "Content-Type, X-API-Version, X-Next-Link, X-Previous-Link"
    response.headers[
        "Access-Control-Allow-Methods"
    ] = "GET, POST, PUT, DELETE, PATCH, OPTIONS"
    return response
