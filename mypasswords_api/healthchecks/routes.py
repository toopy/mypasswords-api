from .views import HealthchecksView


def register_routes(app):
    app.router.add_route("*", "/healthchecks", HealthchecksView)
