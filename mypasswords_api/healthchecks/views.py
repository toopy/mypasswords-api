from aiohttp import web
from asyncpg.exceptions import InterfaceError


class HealthchecksView(web.View):
    async def get(self):
        try:
            await self.request.app.conn.fetch("BEGIN TRANSACTION READ WRITE;")
            return web.json_response({"postgres": "ok"})
        except InterfaceError:
            return web.json_response({"postgres": "error"}, status=500)
