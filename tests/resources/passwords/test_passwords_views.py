import json
from unittest.mock import patch

import jwt
from mypasswords_api.core.exceptions import NotFound, UniqueViolationError
from mypasswords_api.core.password import Password
from simple_settings.utils import settings_stub

PASSWORD_DICT = {
    "id": "b4104cc2-fb3d-4cb4-a9d8-5e225095affc",
    "login": "dummy_login",
    "password": "dummy_password",
    "site": "dummy_site",
}


async def count(*a, **kw):
    return 10


async def null(*a, **kw):
    pass


async def password(*a, **kw):
    return Password(**PASSWORD_DICT.copy())


async def password_list(*a, **kw):
    return [(Password(**PASSWORD_DICT.copy()))]


async def test_delete(client):
    # no pasword id
    response = await client.delete("/passwords")
    assert response.status == 404
    assert (await response.json()) == {"error": {"message": "not-found"}}

    # ok
    with patch(
        "mypasswords_api.core.password.Password.delete", return_value=null()
    ) as mock:
        response = await client.delete("/passwords/dummy_id")
        assert response.status == 204
        assert not (await response.json())
        assert mock.called

    # not found
    with patch("mypasswords_api.core.password.Password.delete", side_effect=NotFound):
        response = await client.delete("/passwords/dummy_id")
        assert response.status == 404
        assert (await response.json()) == {"error": {"message": "not-found"}}


async def test_get(client, server_crt):
    # ok
    with patch(
        "mypasswords_api.core.password.Password.get", side_effect=password
    ) as mock:
        with settings_stub(SERVER_KEY="data/server.key"):
            response = await client.get("/passwords/dummy_id")
            assert response.status == 200
            encoded = (await response.json())["jwt"]
            assert (
                jwt.decode(encoded, server_crt, algorithms="RS256")
                == PASSWORD_DICT.copy()
            )
            assert mock.called

    # not found
    with patch("mypasswords_api.core.password.Password.get", side_effect=NotFound):
        response = await client.get("/passwords/dummy_id")
        assert response.status == 404
        assert (await response.json()) == {"error": {"message": "not-found"}}

    # list
    with patch(
        "mypasswords_api.core.password.Password.list", side_effect=password_list
    ) as mock:
        response = await client.get("/passwords")
        assert response.status == 200
        assert (await response.json()) == [PASSWORD_DICT.copy()]
        assert mock.called


async def test_head(client):
    # ok
    with patch(
        "mypasswords_api.core.password.Password.exist", side_effect=null
    ) as mock:
        response = await client.head("/passwords/dummy_id")
        assert response.status == 200
        assert not (await response.json())
        assert mock.called

    # not found
    with patch("mypasswords_api.core.password.Password.exist", side_effect=NotFound):
        response = await client.head("/passwords/dummy_id")
        assert response.status == 404
        assert not (await response.json())
        assert mock.called

    # list
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=count
    ) as mock:
        response = await client.head("/passwords")
        assert response.status == 200
        assert int(response.headers["X-Count"]) == 10
        assert not (await response.json())
        assert mock.called


async def test_post(client, client_key):
    password_dict = PASSWORD_DICT.copy()
    password_dict.pop("id")

    with patch(
        "mypasswords_api.core.password.Password.create", side_effect=null
    ) as mock:
        with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
            response = await client.post(
                "/passwords",
                data=json.dumps(
                    {
                        "jwt": jwt.encode(
                            password_dict, client_key, algorithm="RS256"
                        ).decode()
                    }
                ),
            )
            assert response.status == 201
            assert (await response.json()) == dict(id=None, **password_dict)
            assert mock.called

    # unique error
    with patch(
        "mypasswords_api.core.password.Password.create",
        side_effect=UniqueViolationError,
    ):
        with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
            response = await client.post(
                "/passwords",
                data=json.dumps(
                    {
                        "jwt": jwt.encode(
                            password_dict, client_key, algorithm="RS256"
                        ).decode()
                    }
                ),
            )
            assert response.status == 400
            assert (await response.json()) == {
                "error": {"message": "unique-constraint-error"}
            }

    # validation error
    with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
        response = await client.post(
            "/passwords",
            data=json.dumps(
                {
                    "jwt": jwt.encode(
                        {"login": "dummy@oops.org"}, client_key, algorithm="RS256"
                    ).decode()
                }
            ),
        )
        assert response.status == 400
        assert (await response.json()) == {
            "error": {
                "fields": {"site": "field required",},
                "message": "validation-error",
            }
        }

    # missing client crt
    with settings_stub(CLIENT_CRT_LIST=""):
        response = await client.post(
            "/passwords",
            data=json.dumps(
                {
                    "jwt": jwt.encode(
                        password_dict, client_key, algorithm="RS256"
                    ).decode()
                }
            ),
        )
        assert response.status == 500
        assert (await response.json()) == {"error": {"message": "server-error"}}

    # not encrypted
    response = await client.post("/passwords", data=json.dumps(password_dict))
    assert response.status == 500
    assert (await response.json()) == {"error": {"message": "server-error"}}


async def test_put(client, client_key):
    password_dict = PASSWORD_DICT.copy()
    password_id = password_dict["id"]

    # create
    with patch(
        "mypasswords_api.core.password.Password.create", side_effect=null
    ) as create_mock:
        with patch(
            "mypasswords_api.core.password.Password.exist", side_effect=NotFound
        ) as exist_mock:
            with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
                response = await client.put(
                    "/passwords/{}".format(password_id),
                    data=json.dumps(
                        {
                            "jwt": jwt.encode(
                                password_dict, client_key, algorithm="RS256"
                            ).decode()
                        }
                    ),
                )
                assert response.status == 201
                assert (await response.json()) == password_dict
                assert create_mock.called
                assert exist_mock.called

    # update
    with patch(
        "mypasswords_api.core.password.Password.update", side_effect=null
    ) as update_mock:
        with patch(
            "mypasswords_api.core.password.Password.exist", side_effect=null
        ) as exist_mock:
            with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
                response = await client.put(
                    "/passwords/{}".format(password_id),
                    data=json.dumps(
                        {
                            "jwt": jwt.encode(
                                password_dict, client_key, algorithm="RS256"
                            ).decode()
                        }
                    ),
                )
                assert response.status == 200
                assert (await response.json()) == password_dict
                assert update_mock.called
                assert exist_mock.called

    # unique error
    with patch(
        "mypasswords_api.core.password.Password.update",
        side_effect=UniqueViolationError,
    ):
        with patch(
            "mypasswords_api.core.password.Password.exist", side_effect=null
        ) as exist_mock:
            with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
                response = await client.put(
                    "/passwords/{}".format(password_id),
                    data=json.dumps(
                        {
                            "jwt": jwt.encode(
                                password_dict, client_key, algorithm="RS256"
                            ).decode()
                        }
                    ),
                )
                assert response.status == 400
                assert (await response.json()) == {
                    "error": {"message": "unique-constraint-error"}
                }

    # validation error
    with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
        response = await client.put(
            "/passwords/{}".format(password_id),
            data=json.dumps(
                {
                    "jwt": jwt.encode(
                        {"login": "dummy@oops.org"}, client_key, algorithm="RS256"
                    ).decode()
                }
            ),
        )
        assert response.status == 400
        assert (await response.json()) == {
            "error": {
                "fields": {"site": "field required",},
                "message": "validation-error",
            }
        }

    # missing client crt
    with settings_stub(CLIENT_CRT_LIST=""):
        response = await client.put(
            "/passwords/{}".format(password_id),
            data=json.dumps(
                {
                    "jwt": jwt.encode(
                        password_dict, client_key, algorithm="RS256"
                    ).decode()
                }
            ),
        )
        assert response.status == 500
        assert (await response.json()) == {"error": {"message": "server-error"}}

    # not encrypted
    response = await client.put(
        "/passwords/{}".format(password_id), data=json.dumps(password_dict),
    )
    assert response.status == 500
    assert (await response.json()) == {"error": {"message": "server-error"}}
