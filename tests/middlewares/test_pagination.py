from unittest.mock import call, patch

from mypasswords_api.middlewares.pagination import get_link, pagination_middleware
from mypasswords_api.testing import AsyncMock, FakeRequest, FakeResponse


async def fake_count(*a, **kw):
    return 40


async def test_get_link_previous():

    # no page
    request = FakeRequest()
    assert not await get_link(request, kind="previous")

    # ok
    request = FakeRequest(page=1)
    assert (
        await get_link(request, kind="previous")
        == "/passwords?direction=asc&order_by=site&page=0&per_page=25"
    )


async def test_get_link_next():

    # ok
    request = FakeRequest()
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        assert (
            await get_link(request)
            == "/passwords?direction=asc&order_by=site&page=1&per_page=25"
        )
        assert mock.call_args_list == [call("dummy_conn", q=None)]

    # ok with q
    request = FakeRequest(query={"q": "dUm"})

    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        assert (
            await get_link(request)
            == "/passwords?direction=asc&order_by=site&page=1&per_page=25"
        )
        assert mock.call_args_list == [call("dummy_conn", q="dUm")]

    # no next link based on count
    request = FakeRequest(per_page=40)
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        assert not await get_link(request)
        assert mock.call_args_list == [call("dummy_conn", q=None)]

    # no next link based on count and page
    request = FakeRequest(page=1)
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        assert not await get_link(request)
        assert mock.call_args_list == [call("dummy_conn", q=None)]


async def test_pagination_middleware():

    # handler mock
    handler = AsyncMock(return_value=FakeResponse())

    # default behavior
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        response = await pagination_middleware(FakeRequest(), handler)
        assert response.headers == {
            "X-Next-Link": "/passwords?direction=asc&order_by=site&page=1&per_page=25",
        }
        assert mock.call_args_list == [call("dummy_conn", q=None)]

    # new handler mock
    handler = AsyncMock(return_value=FakeResponse())

    # no next if per_page 50
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        response = await pagination_middleware(
            FakeRequest(query={"per_page": 50}), handler,
        )
        assert not response.headers
        assert mock.call_args_list == [call("dummy_conn", q=None)]

    # new handler mock
    handler = AsyncMock(return_value=FakeResponse())

    # page 1
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        response = await pagination_middleware(FakeRequest(query={"page": 1}), handler,)
        assert response.headers == {
            "X-Previous-Link": (
                "/passwords?direction=asc&order_by=site&page=0&per_page=25"
            ),
        }
        assert mock.call_args_list == [call("dummy_conn", q=None)]

    # new handler mock
    handler = AsyncMock(return_value=FakeResponse())

    # previous and next
    with patch(
        "mypasswords_api.core.password.Password.count", side_effect=fake_count
    ) as mock:
        response = await pagination_middleware(
            FakeRequest(query={"page": 1, "per_page": 10}), handler,
        )
        assert response.headers == {
            "X-Next-Link": (
                "/passwords?direction=asc&order_by=site&page=2&per_page=10"
            ),
            "X-Previous-Link": (
                "/passwords?direction=asc&order_by=site&page=0&per_page=10"
            ),
        }
        assert mock.call_args_list == [call("dummy_conn", q=None)]
