import jwt
import pytest
from jwt.exceptions import DecodeError
from mypasswords_api.core.exceptions import ServerError
from mypasswords_api.core.jwt import (
    decode_jwt,
    encode_jwt,
    get_client_crt_list,
    get_server_key,
)
from simple_settings.utils import settings_stub


async def test_get_client_crt_list():
    with settings_stub(CLIENT_CRT_LIST="data/dummy.crt"):
        with pytest.raises(ServerError):
            await get_client_crt_list()

    with settings_stub(CLIENT_CRT_LIST="data/client.crt,data/dummy.crt"):
        client_crt_list = await get_client_crt_list()
        assert len(client_crt_list) == 1
        assert client_crt_list[0].startswith("-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSq")


async def test_server_key():
    # not set
    with settings_stub(SERVER_KEY=""):
        with pytest.raises(ServerError):
            await get_server_key()

    # not exist
    with settings_stub(SERVER_KEY="data/dummy.key"):
        with pytest.raises(ServerError):
            await get_server_key()

    # ok
    with settings_stub(SERVER_KEY="data/server.key"):
        server_key = await get_server_key()
        assert server_key.startswith("-----BEGIN RSA PRIVATE KEY-----\nMIICXAIBAAKBgQ")


async def test_decode_jwt(client_key):
    data = jwt.encode({"foo": "bar"}, client_key, algorithm="RS256")
    # no crt
    with settings_stub(CLIENT_CRT_LIST=""):
        with pytest.raises(ServerError):
            await decode_jwt(data)
    # cert not found
    with settings_stub(CLIENT_CRT_LIST="data/dummy.crt"):
        with pytest.raises(ServerError):
            await decode_jwt(data)
    # not encrypted
    with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
        with pytest.raises(DecodeError):
            await decode_jwt({"foo": "bar"})
    # ok
    with settings_stub(CLIENT_CRT_LIST="data/client.crt"):
        await decode_jwt(data)


async def test_encode_jwt():
    # no key
    with settings_stub(SERVER_KEY=""):
        with pytest.raises(ServerError):
            await encode_jwt({"foo": "bar"})
    # key not found
    with settings_stub(SERVER_KEY="data/dummy.key"):
        with pytest.raises(ServerError):
            await encode_jwt({"foo": "bar"})
    # ok
    with settings_stub(SERVER_KEY="data/server.key"):
        await encode_jwt({"foo": "bar"})
