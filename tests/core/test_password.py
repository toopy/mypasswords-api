from unittest.mock import MagicMock, call
from uuid import uuid4

import pytest
from mypasswords_api.core.exceptions import NotFound
from mypasswords_api.core.password import Password, Uuid
from mypasswords_api.testing import AsyncMock
from pydantic import BaseModel


class Dummy(BaseModel):
    id: Uuid = None


async def test_uuid_field():

    value = uuid4()
    assert Dummy(id=value).id == value

    # from str
    value = "28abf830-b179-415a-8257-1652415ac035"
    assert Dummy(id=value).id == value

    # invalid type
    with pytest.raises(ValueError):
        Dummy(id=0)

    # invalid str
    with pytest.raises(ValueError):
        Dummy(id="abcd")


async def test_password_count():

    conn = MagicMock()
    conn.fetchrow = AsyncMock(return_value={"count": 1})

    # count all
    assert await Password.count(conn) == 1
    assert conn.fetchrow.call_args_list == [call("SELECT count(*) FROM passwords;")]

    # reset mock
    conn.fetchrow.reset_mock()

    # count filtered results
    assert await Password.count(conn, q="duM") == 1
    assert conn.fetchrow.call_args_list == [
        call("SELECT count(*) FROM passwords WHERE q @@ 'dum:*'::tsquery;")
    ]


async def test_password_create():

    password_dict = {
        "login": "dummy_login",
        "password": "dummy_password",
        "site": "dummy_site",
    }

    # prepare mock
    conn = MagicMock()
    conn.execute = AsyncMock()

    # create
    password = Password(**password_dict)
    await password.create(conn)

    assert conn.execute.call_args_list == [
        call(
            "INSERT INTO passwords (id, login, password, site) "
            "VALUES ($1, $2, $3, $4);",
            str(password.id),
            "dummy_login",
            "dummy_password",
            "dummy_site",
        )
    ]


async def test_password_create_with_id():

    password_dict = {
        "id": "fae9cbeb-40cc-42eb-8ca0-1ca12138353b",
        "login": "dummy_login",
        "password": "dummy_password",
        "site": "dummy_site",
    }

    # prepare mock
    conn = MagicMock()
    conn.execute = AsyncMock()

    # create
    password = Password(**password_dict)
    await password.create(conn)

    assert conn.execute.call_args_list == [
        call(
            "INSERT INTO passwords (id, login, password, site) "
            "VALUES ($1, $2, $3, $4);",
            "fae9cbeb-40cc-42eb-8ca0-1ca12138353b",
            "dummy_login",
            "dummy_password",
            "dummy_site",
        )
    ]


async def test_password_delete():

    conn = MagicMock()
    conn.execute = AsyncMock(return_value="DELETE 1")

    # ok
    assert not await Password.delete(conn, "dummy")
    assert conn.execute.call_args_list == [
        call("DELETE FROM passwords WHERE id = $1;", "dummy")
    ]

    # reset mock
    conn.execute.reset_mock()
    conn.execute = AsyncMock(return_value="DELETE 0")

    # not found
    with pytest.raises(NotFound):
        await Password.delete(conn, "dummy")
    assert conn.execute.call_args_list == [
        call("DELETE FROM passwords WHERE id = $1;", "dummy")
    ]

    # reset mock
    conn.execute.reset_mock()
    conn.execute = AsyncMock(side_effect=ValueError)

    # invalid id
    with pytest.raises(NotFound):
        await Password.delete(conn, "dummy")
    assert conn.execute.call_args_list == [
        call("DELETE FROM passwords WHERE id = $1;", "dummy")
    ]


async def test_password_exist():

    conn = MagicMock()
    conn.fetchrow = AsyncMock(return_value=True)

    # ok
    assert await Password.exist(conn, "dummy")
    assert conn.fetchrow.call_args_list == [
        call("SELECT 1 FROM passwords WHERE id = $1 LIMIT 1;", "dummy")
    ]

    # reset mock
    conn.fetchrow.reset_mock()
    conn.fetchrow = AsyncMock(return_value=False)

    # not found
    with pytest.raises(NotFound):
        await Password.exist(conn, "dummy")
    assert conn.fetchrow.call_args_list == [
        call("SELECT 1 FROM passwords WHERE id = $1 LIMIT 1;", "dummy")
    ]

    # reset mock
    conn.fetchrow.reset_mock()
    conn.fetchrow = AsyncMock(side_effect=ValueError)

    # invalid id
    with pytest.raises(NotFound):
        await Password.exist(conn, "dummy")
    assert conn.fetchrow.call_args_list == [
        call("SELECT 1 FROM passwords WHERE id = $1 LIMIT 1;", "dummy")
    ]


async def test_password_get():

    password_dict = {
        "id": uuid4(),
        "login": "dummy_login",
        "password": "dummy_password",
        "site": "dummy_site",
    }

    conn = MagicMock()
    conn.fetchrow = AsyncMock(return_value=password_dict)

    # ok
    result = await Password.get(conn, "dummy")
    assert isinstance(result, Password)
    assert result.id == password_dict["id"]
    assert conn.fetchrow.call_args_list == [
        call("SELECT * FROM passwords WHERE id = $1;", "dummy")
    ]

    # reset mock
    conn.fetchrow.reset_mock()
    conn.fetchrow = AsyncMock(return_value={})

    # not found
    with pytest.raises(NotFound):
        await Password.get(conn, "dummy")
    assert conn.fetchrow.call_args_list == [
        call("SELECT * FROM passwords WHERE id = $1;", "dummy")
    ]

    # reset mock
    conn.fetchrow.reset_mock()
    conn.fetchrow = AsyncMock(side_effect=ValueError)

    # invalid id
    with pytest.raises(NotFound):
        await Password.get(conn, "dummy")
    assert conn.fetchrow.call_args_list == [
        call("SELECT * FROM passwords WHERE id = $1;", "dummy")
    ]


async def test_password_list():

    password_dict = {
        "id": uuid4(),
        "login": "dummy_login",
        "password": "dummy_password",
        "site": "dummy_site",
    }

    conn = MagicMock()
    conn.fetch = AsyncMock(return_value=[password_dict])

    # ok - default
    result = (await Password.list(conn))[0]
    assert isinstance(result, Password)
    assert result.id == password_dict["id"]
    assert conn.fetch.call_args_list == [
        call(
            "SELECT id, login, site "
            "FROM passwords "
            "ORDER BY (site, login) ASC "
            "OFFSET 0 "
            "LIMIT 25;",
        )
    ]

    # reset mock
    conn.fetch.reset_mock()

    # with q
    result = (await Password.list(conn, q="duM"))[0]
    assert isinstance(result, Password)
    assert result.id == password_dict["id"]
    assert conn.fetch.call_args_list == [
        call(
            "SELECT id, login, site "
            "FROM passwords "
            "WHERE q @@ $1::tsquery "
            "ORDER BY (site, login) ASC "
            "OFFSET 0 "
            "LIMIT 25;",
            "dum:*",
        )
    ]

    # reset mock
    conn.fetch.reset_mock()

    # order by login - desc
    result = (await Password.list(conn, direction="DESC", order_by="login"))[0]
    assert isinstance(result, Password)
    assert result.id == password_dict["id"]
    assert conn.fetch.call_args_list == [
        call(
            "SELECT id, login, site "
            "FROM passwords "
            "ORDER BY (login, site) DESC "
            "OFFSET 0 "
            "LIMIT 25;",
        )
    ]

    # reset mock
    conn.fetch.reset_mock()

    # page 2 + per_page 100
    result = (await Password.list(conn, page=2, per_page=100))[0]
    assert isinstance(result, Password)
    assert result.id == password_dict["id"]
    assert conn.fetch.call_args_list == [
        call(
            "SELECT id, login, site "
            "FROM passwords "
            "ORDER BY (site, login) ASC "
            "OFFSET 200 "
            "LIMIT 100;",
        )
    ]


async def test_password_to_dict():

    password_dict = {
        "id": str(uuid4()),
        "login": "dummy_login",
        "password": "dummy_password",
        "site": "dummy_site",
    }

    assert Password(**password_dict).to_dict() == password_dict


async def test_password_update():

    password_id = uuid4()
    password_dict = {
        "login": "dummy_login",
        "password": "dummy_password",
        "site": "dummy_site",
    }

    conn = MagicMock()
    conn.execute = AsyncMock(return_value="UPDATE 1")

    # ok
    assert not await Password(**password_dict).update(conn, password_id)
    assert conn.execute.call_args_list == [
        call(
            "UPDATE passwords "
            "SET login = $1, password = $2, site = $3 "
            "WHERE id = $4;",
            "dummy_login",
            "dummy_password",
            "dummy_site",
            password_id,
        )
    ]

    # reset mock
    conn.execute.reset_mock()
    conn.execute = AsyncMock(return_value="UPDATE 0")

    # not found
    with pytest.raises(NotFound):
        await Password(**password_dict).update(conn, password_id)
    assert conn.execute.call_args_list == [
        call(
            "UPDATE passwords "
            "SET login = $1, password = $2, site = $3 "
            "WHERE id = $4;",
            "dummy_login",
            "dummy_password",
            "dummy_site",
            password_id,
        )
    ]

    # reset mock
    conn.execute.reset_mock()
    conn.execute = AsyncMock(side_effect=ValueError)

    # invalid id
    with pytest.raises(NotFound):
        await Password(**password_dict).update(conn, password_id)
    assert conn.execute.call_args_list == [
        call(
            "UPDATE passwords "
            "SET login = $1, password = $2, site = $3 "
            "WHERE id = $4;",
            "dummy_login",
            "dummy_password",
            "dummy_site",
            password_id,
        )
    ]
