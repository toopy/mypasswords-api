import pytest


@pytest.fixture
async def close_conn(app):
    await app.conn.close()
    yield


async def test_healthcheck_success(client):

    response = await client.get("/healthchecks")
    assert response.status == 200

    content = await response.json()
    assert content["postgres"] == "ok"


async def test_healthcheck_fail(app, client, close_conn):

    await app.conn.close()

    response = await client.get("/healthchecks")
    assert response.status == 500
