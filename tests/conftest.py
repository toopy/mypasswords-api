import aiofiles
import pytest
from mypasswords_api.app import build_app, load_plugins


@pytest.fixture
def app():
    return build_app()


@pytest.fixture
def client(aiohttp_client, app, loop):
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture(autouse=True)
def register_plugins(app, loop):
    loop.run_until_complete(load_plugins(app))


@pytest.fixture
async def client_key():
    async with aiofiles.open("data/client.key") as f:
        return await f.read()


@pytest.fixture
async def server_crt():
    async with aiofiles.open("data/server.crt") as f:
        return await f.read()
